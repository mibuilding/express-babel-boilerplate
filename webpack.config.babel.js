import path from 'path'
// import nodeExternals from 'webpack-node-externals'

const config = {
  mode  : 'production',
  entry : './index.js',
  target: 'async-node',
  module: {
    rules: [
      {
        test   : /\.(js)?$/,
        use    : ['babel-loader'],
        exclude: /node_modules/
      }
    ]
  },
  resolve: {
    extensions: ['*', '.js']
  },
  output: {
    filename: 'api.js',
    path    : path.resolve(__dirname, 'dist')
    }
//   externals: nodeExternals()
}

export default config
