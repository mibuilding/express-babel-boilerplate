export default {
    'verbose'                   : true,
    'testEnvironment'           : 'node',
    'coveragePathIgnorePatterns': [
        '/node_modules/'
    ]

}
