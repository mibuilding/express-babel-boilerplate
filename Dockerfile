FROM node:13-alpine as base
WORKDIR /api
COPY . .
RUN npm install
RUN npm run build

FROM node:13-alpine
WORKDIR /api
ENV NODE_ENV=production
RUN chown node:node .
USER node
COPY --from=base /api/dist/api.js .
CMD ["node", "api.js"]