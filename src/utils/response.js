export default (statusCode = 500, res) => {
        // defining body of the response
        const body = {
            code       : statusCode,
            error      : isError(statusCode),
            err_message: getErrorMessage(statusCode),
            data       : res
        }

        return body
}

const isError =  code => {
    return code >= 200 && code <= 299 ? false : true
}

const getErrorMessage = code => {
    return code in errors ? errors[code] : ''
}

const errors = {
        200: 'OK',
        201: 'Created',
        202: 'Accepted for delivery',
        400: 'Bad Request',
        401: 'Unauthorised',
        402: 'Payment Required',
        403: 'Forbidden',
        404: 'Not Found',
        500: 'Internal Server Error. Please contact support team for assistance.'
}
