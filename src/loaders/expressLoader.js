import express from 'express'
import cors from 'cors'
import hpp from 'hpp'
import routes from '../controllers'
import helmet from 'helmet'

export default async app => {

    // TODO: middlware should register here
    app.use(helmet())
    app.use(express.urlencoded({
        extended: true
    }))
    app.use(express.json())
    app.use(cors())
    app.use(hpp())

    // importing routes
    app.use(routes)
}
