import { EventEmitter } from 'events'
import TestEventListener from '../subscribers/helloEvent'

const emitter = new EventEmitter()

export default async app => {
    Promise.all([TestEventListener(emitter)])
    app.locals.emitter = emitter
}
