import expressLoader from './expressLoader'
import subscribers from '../subscribers'

export default async express => {

    // loading subscribers for events listening
    await subscribers()

    // loading express configuration
    await expressLoader(express)
}
