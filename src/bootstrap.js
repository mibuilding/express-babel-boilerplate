import express from 'express'
import loaders from './loaders'

// Loading initialisers
export default async () => {
    const app = express()

    // loaders
    await loaders(app)

    app.listen(3000, () => {
        console.log('running')
    })
}
