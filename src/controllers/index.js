import { Router } from 'express'
import homeRoute from './routes/home.routes'

const app = Router()

app.use('/', homeRoute)

export default app
