import { Router } from 'express'
import { rootMessage } from '../../config/messages'
import response from '../../utils/response'
import emitter from '../../utils/emitter'
const router = Router()


router.get('/', async (req, res) => {
    
    // sending emit to listener
    emitter.emit('hello')

    // returning response
    return res.status(200).json(response(200, rootMessage))
})

export default router
