import { hostname } from 'os'

export default {
    SERVER_NAME: hostname(),
    PORT       : process.env.PORT || 3000,
    NODE_ENV   : process.env.NODE_ENV || 'development'
}
