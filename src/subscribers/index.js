import helloEventListener from './helloEvent'

export default async () => {
    return Promise.all([
        helloEventListener()
    ])
}
