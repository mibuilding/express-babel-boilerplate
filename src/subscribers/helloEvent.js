import emitter from '../utils/emitter'

export default async () => {

    console.log('Listener registered for hello')
    emitter.on('hello', () => {
        console.log('Welcome!')
    })
}
